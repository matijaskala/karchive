/* This file is part of the KDE libraries
   SPDX-FileCopyrightText: 2023 Matija Skala <mskala@gmx.com>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "kbrfilter.h"
#include "loggingcategory.h"

#if HAVE_BR_SUPPORT

#include <libbr.h>

#include <QDebug>

#include <QIODevice>

class Q_DECL_HIDDEN KBrFilter::Private
{
public:
    Private()
        : isInitialized(false)
    {
        mode = 0;
    }

    bool isInitialized;
    int mode;
    br_stream* brStream;
};

KBrFilter::KBrFilter()
    : d(new Private)
{
    d->brStream = br_init();
}

KBrFilter::~KBrFilter()
{
    br_fini(d->brStream);
    delete d;
}

bool KBrFilter::init(int mode)
{
    if (mode == QIODevice::ReadOnly) {
    } else if (mode == QIODevice::WriteOnly) {
    } else {
        // qCWarning(KArchiveLog) << "Unsupported mode " << mode << ". Only QIODevice::ReadOnly and QIODevice::WriteOnly supported";
        return false;
    }
    d->mode = mode;
    d->isInitialized = true;
    return true;
}

int KBrFilter::mode() const
{
    return d->mode;
}

bool KBrFilter::terminate()
{
    d->isInitialized = false;
    return true;
}

void KBrFilter::reset()
{
}

void KBrFilter::setOutBuffer(char *data, unsigned int maxlen)
{
    br_set_out_buffer(d->brStream, data, maxlen);
}

void KBrFilter::setInBuffer(const char *data, unsigned int size)
{
    br_set_in_buffer(d->brStream, data, size);
}

int KBrFilter::inBufferAvailable() const
{
    return br_in_buffer_available(d->brStream);
}

int KBrFilter::outBufferAvailable() const
{
    return br_out_buffer_available(d->brStream);
}

bool KBrFilter::readHeader()
{
    return br_read_header(d->brStream) == BR_OK;
}

bool KBrFilter::writeHeader(const QByteArray &filename)
{
    return br_write_header(d->brStream, filename.isNull() ? nullptr : filename.constData()) == BR_OK;
}

KBrFilter::Result KBrFilter::uncompress()
{
    switch (br_uncompress(d->brStream)) {
        case BR_ERROR:
            return KFilterBase::Error;
        case BR_OK:
            return KFilterBase::Ok;
        case BR_END:
            return KFilterBase::End;
    }
    return KFilterBase::Error;
}

KBrFilter::Result KBrFilter::compress(bool finish)
{
    switch (br_compress(d->brStream, finish)) {
        case BR_ERROR:
            return KFilterBase::Error;
        case BR_OK:
            return KFilterBase::Ok;
        case BR_END:
            return KFilterBase::End;
    }
    return KFilterBase::Error;
}

#endif /* HAVE_BR_SUPPORT */
