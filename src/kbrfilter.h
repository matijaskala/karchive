/* This file is part of the KDE libraries
   SPDX-FileCopyrightText: 2023 Matija Skala <mskala@gmx.com>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef __kbrfilter__h
#define __kbrfilter__h

#include <config-compression.h>

#if HAVE_BR_SUPPORT

#include "kfilterbase.h"

/**
 * Internal class used by KCompressionDevice
 * @internal
 */
class KBrFilter : public KFilterBase
{
public:
    KBrFilter();
    ~KBrFilter() override;

    bool init(int) override;
    int mode() const override;
    bool terminate() override;
    void reset() override;
    bool readHeader() override;
    bool writeHeader(const QByteArray &) override;
    void setOutBuffer(char *data, unsigned int maxlen) override;
    void setInBuffer(const char *data, unsigned int size) override;
    int inBufferAvailable() const override;
    int outBufferAvailable() const override;
    Result uncompress() override;
    Result compress(bool finish) override;

private:
    class Private;
    Private *const d;
};

#endif

#endif
